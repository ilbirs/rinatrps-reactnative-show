import React, {useContext, useEffect, useState} from 'react';
import {FlatList, Text, TouchableOpacity, View} from "react-native";
import ContextGlobal from "./context/ContextToken";
import {getGameList, joinGame} from "./api/MyApi";
import {useFonts} from "expo-font";
import styles from './style/JoinStyle.js'

export default function Join({navigation}) {
    const [gameList, setListGame] = useState([]);
    const token = useContext(ContextGlobal);
    const [loaded] = useFonts({
        Pattaya: require('../assets/fonts/Pattaya-Regular.ttf'),
    });

    useEffect(() => {
        getGameList(token)
            .then((response) => response.json())
            .then((res) => setListGame(res))
    }, []);
    console.log('gameList', gameList)

    if (!loaded) {
        return null;
    }

    return (
        <View style={styles.block}>
            <View style={styles.block2}>
                <FlatList style={styles.input}
                          keyExtractor={item => item.id}
                          data={gameList}
                          renderItem={({item}) => (
                              <TouchableOpacity onPress={() => {
                                  navigation.navigate('Game')
                                  joinGame(token,item.id)
                              }} style={styles.nameInput}>
                                  <Text  style={styles.txn}>{item.player ? item.player :'anonymous'}</Text>
                              </TouchableOpacity>
                          )}
                />
            </View>
        </View>
    )
}


/*
const [value, setValue] = useState('');
const pressHandler =() => {
    if(value.trim()) {
        onSubmit(value)
        setValue('')
    }else {
        //ios  Alert.alert('field can not be blank')
        alert('field can not be blank')
    }
    /!*  if(value) {
          onSubmit(value)
          setValue('')
      }else {
          throw new Error()
      }*!/
}*/
/*
<View style={styles.block}>
    <TextInput style={styles.input}
               onChangeText={text=> setValue(text)}
               value={value}
               placeholder={'add item'}
               autoCorrect={false}//ne sorteruet pravilno slova
               keyboardType='web-search'
    />
    <Button title='add' onPress={pressHandler}/>
</View>*/
/*
const [gameList, setListGame] = useState();
const [nameTillToken, setNameTillToken] = useState('');
const token = useContext(ContextGlobal);



const pressHandler = () => {
    const requestOptions = {
        headers: {
            "token": token
            , "Content-Type": "application/json;"
        },
    };
    fetch("http://localhost:8080/games",requestOptions)
        .then(response => response.json())
        .then(res => setListGame(res))
        .catch((error) => console.error(error))
}
console.log('game', gameList)*/
{/*     <View style={styles.block2}>
                <TextInput style={styles.input}
                           onChangeText={text=> setNameTillToken(text)}
                           value={nameTillToken}
                      onChangeText={itemInputHandler}
                           placeholder={'Play with Name'}
                           placeholderTextColor='black'

                />
                <RectButton style={styles.button} activeOpacity={0.5} onPress={pressHandler3} >
                    <Icon name="arrow-right" style={styles.icon}/>
                </RectButton>

            </View>*/}
/*  const pressHandler3 = () => {
       pressHandler2()
       navigation.navigate('GamePage')
   }*/

/*
    useEffect(() => {
        const requestOptions = {
            headers: {
                "token": token
                , "Content-Type": "application/json;"
            },
        };
        fetch('http://192.168.0.173:8080/games',requestOptions)
            .then((response) => response.json())
            .then((res) => setListGame(res))
            .catch((error) => console.error(error))
    }, []);
    console.log('gameList', gameList)
*/

/*  const pressHandler2 = (gameId) => {
         const requestOptions = {
             headers: {
                 "token": token
                 , "Content-Type": "application/json;"
             },
         };

         fetch(`http://192.168.0.173:8080/games/join/${gameId}`, requestOptions)
             .then((response) => response.json())
             .then((res) => res);
     }*/
/*.then((response) => response.json())
        .then((res) => res)*/
/*  console.log('game', gameJoin)*/
