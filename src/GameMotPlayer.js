
import React, {useContext} from 'react';
import {ImageBackground, Text, TouchableOpacity, View} from 'react-native';
import ContextGlobal from "./context/ContextToken";
import {startGame} from "./api/MyApi";
import styles from './style/MotPlayerStyle.js'

const image = {uri:  "https://previews.123rf.com/images/xmee/xmee1501/xmee150100080/35153838-rock-paper-scissors.jpg" }

export default function GameMotPlayer({navigation}) {
    const token = useContext(ContextGlobal);

    const pressHandler = () => {
        startGame(token)
        navigation.navigate('Game');
    }

    return (
        <ImageBackground source={image} style={styles.image}>
        <View style={styles.container}>
            <View style={styles.butt}>
                <TouchableOpacity style={styles.create}
                                  onPress={pressHandler}>
                    <Text style={styles.textCreate}>Start Game</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.join} title='Join'
                                  onPress={()=> navigation.navigate('Join')}>
                    <Text style={styles.textJoin}>Join Game</Text>
                </TouchableOpacity>
            </View>
        </View>
     </ImageBackground>

    );
}



/* const pressHandler2 = () => {
     const requestOptions = {
         headers: {
             "token": token
             , "Content-Type": "application/json;"
         },
     };

     fetch(`http://192.168.0.173:8080/games/start`,requestOptions)
         .then((response) => response.json())
         .then((res) => setGame(res))
         .catch((error) => console.error(error))
 }*/
/*  console.log('game', game)*/
/* <StatusBar style="auto" />*/
