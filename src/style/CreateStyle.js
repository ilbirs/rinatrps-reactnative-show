import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    block:{
        justifyContent:'space-around',
        alignItems:'center',
        marginBottom:15,
        paddingHorizontal:20,
        paddingVertical:20
    },
    block2:{
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
        marginBottom:15,
        paddingHorizontal:20,
        paddingVertical:20
    },
    input:{
        width:'60%',
        padding:10,
        borderStyle:'solid',
        borderBottomWidth:2,
        borderBottomColor:'#3949ab',
        color:'white',
        backgroundColor:'#800000',
        borderRadius:10,
        opacity: 0.7,
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    icon:{
        color:'#fff',
        fontSize:30,
    },
    button: {
        backgroundColor: '#00BFFF',
        height: 38,
        flexDirection: 'row',
        borderRadius: 8,
        alignItems: 'center',
        marginTop: 8,
        top:-4
    },
    createAnonymous:{
        borderWidth:2,
        borderRadius:10,
        width:210,
        height:50,
        opacity: 0.9,
        backgroundColor: '#2F4F4F'
    },
    textAnonymous:{
        position:'absolute',
        top:5,
        left:14,
        fontSize:24,
        padding:3,
        color:'white'
    },
    playOnline:{
        borderWidth:2,
        borderRadius:10,
        width:230,
        height:50,
        opacity: 0.9,
        backgroundColor: '#2F4F4F'
    },
    textPlayOnline:{
        position:'absolute',
        top:5,
        left:25,
        fontSize:24,
        padding:3,
        color:'white'
    },
});
