import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    butt:{
        flexDirection:'column'
    },
    create:{
        borderWidth:2,
        borderRadius:10,
        width:210,
        height:50,
        opacity: 0.9,
        backgroundColor: '#2F4F4F'
    },
    textCreate:{
        position:'absolute',
        top:5,
        left:46,
        fontSize:24,
        padding:3,
        color:'white'
    },
    join:{
        top:40,
        borderWidth:2,
        borderRadius:10,
        width:210,
        height:50,
        opacity: 0.9,
        backgroundColor: '#808000'
    },
    textJoin:{
        position:'absolute',
        top:5,
        left:46,
        fontSize:24,
        padding:3,
        color:'white'
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
});
