import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    butt:{
        flexDirection:'column'
    },
    playOnline:{
        borderWidth:2,
        borderRadius:10,
        width:230,
        height:50,
        opacity: 0.9,
        backgroundColor: '#2F4F4F'
    },
    textPlayOnline:{
        position:'absolute',
        top:5,
        left:56,
        fontSize:24,
        padding:3,
        color:'white'
    },
    playComp:{
        top:40,
        borderWidth:2,
        borderRadius:10,
        width:230,
        height:50,
        opacity: 0.9,
        backgroundColor: '#808000'
    },
    textPlayComp:{
        position:'absolute',
        top:5,
        left:35,
        fontSize:24,
        padding:3,
        color:'white'
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
});
