import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    block:{
        justifyContent:'space-between',
        alignItems:'center',
        marginBottom:15,
        paddingHorizontal:20,
        paddingVertical: 50,
        backgroundColor: '#2F4F4F'
    },
    block2:{
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
        marginBottom:15,
        paddingHorizontal:20,
        paddingVertical:20
    },
    input:{
        width:'60%',
        padding:10,
        borderStyle:'solid',
        borderBottomWidth:2,
        borderBottomColor:'#3949ab'
    },
    icon:{
        color:'#fff',
        fontSize:30,
    },
    button: {
        backgroundColor: '#00BFFF',
        height: 38,
        flexDirection: 'row',
        borderRadius: 8,
        alignItems: 'center',
        marginTop: 8,
        /*      right:30,*/
        top:-4
    },
    nameInput: {
        backgroundColor: "#ff2e4c",
        borderRadius: 5,
        color: "#ffffff",
        minWidth: 60,
        padding: 10,
        margin: 10,
        fontSize: 20,
        fontWeight: "400",
        textAlign: "center"
    },
    txn:{
        fontFamily: 'Pattaya',
        fontSize:25,
        left:60,
        color:'white'
    }
});
