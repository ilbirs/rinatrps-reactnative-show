import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    navbar:{
        height:70,
        alignItems:'center',
        justifyContent:'flex-end',
        backgroundColor:'#3949ab',
        paddingBottom:10
    },
    text:{
        color:'white',
        fontSize:20
    }
});
