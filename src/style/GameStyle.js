import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#F0E68C'
    },
    content: {
        flex: 1,
        padding: 15,
    },
    screen: {
        flex: 1,
        flexDirection: "row",
        left:65,
        top:-40
    },
    readyText: {
        marginTop: -48,
        alignSelf: "center",
        textAlign: "center",
        width: "100%",
        fontSize: 48,
        fontWeight: "bold",
    },
    column: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    playerYou: {
        fontFamily: 'Oi',
        color: "black",
        fontSize: 15,
        marginTop: 5,
        left: 45
    },
    playerImpl: {
        fontFamily: 'Oi',
        color: "black",
        fontSize: 15,
        marginTop: 5,
        left: 7
    },
    playerOpponent: {
        fontFamily: 'Oi',
        color: "black",
        fontSize: 15,
        marginTop: 5,
        left: 17
    },
    designYou: {
        backgroundColor: "#ff2e4c",
        borderRadius: 5,
        height: 35,
        width:125,
        top:285,
        right:83
    },
    designOpponent: {
        backgroundColor: "#ff2e4c",
        borderRadius: 5,
        height: 35,
        width:125,
        top:285,
        right:90
    },
    result: {
        height: 70,
        justifyContent: "flex-end",
        alignItems: "center",
        borderWidth:1
    },
    resultText: {
        fontSize: 30,
        color:'black',
        top:-15
    },
    nameName:{
        flexDirection: 'row',
        justifyContent: "space-around",
        alignItems: 'center',
        top:70,
        left:5
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
        top: 11
    },
    modalView: {
        flexDirection: "row",
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        width: 300,
        height: 150,
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    textStyle: {
        color: "black",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    button1: {
        opacity: 0.9,
        borderWidth: 6,
        borderRadius: 5,
        backgroundColor: 'red',
        borderColor: 'red'
    },
    button2: {
        opacity: 0.9,
        left: 40,
        borderWidth: 6,
        borderRadius: 5,
        backgroundColor: '#4169E1',
        borderColor: '#4169E1'
    },
    columnDisplay: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    playerName: {
        color: "black",
        fontSize: 16,
        marginTop: 16,
    },
    actionButton: {
        width: 100,
        height: 100,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#f9d835",
        borderRadius: 52,
    },
    actions: {
        top:-40,
        height: 100,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        borderBottomWidth:2
    },
    actionButton1: {
        width: 64,
        height: 64,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 32,
        borderColor:'blue',
        borderWidth:7,
    },
    actionButton2: {
        width: 64,
        height: 64,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 32,
        borderColor:'green',
        borderWidth:7
    },
    actionButton3: {
        width: 64,
        height: 64,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 32,
        borderColor:'red',
        borderWidth:7
    },
});
