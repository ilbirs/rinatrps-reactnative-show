import React from "react";
import {Image, View} from "react-native";
import styles from './style/GameStyle.js'

export default function DisplayResult2({ userChoice }) {
    return (
        <>
            <View style={styles.columnDisplay}>
                <Image
                    style={styles.actionButton}
                    source={userChoice}
                />
            </View>
        </>
    );
}

