import React from 'react';
import { Text, View} from "react-native";
import styles from './style/GameStyle.js'

export default function Result({props}) {
    return(
        <View style={styles.result}>
            <Text style={styles.resultText}>{props}</Text>
        </View>
    )
}

