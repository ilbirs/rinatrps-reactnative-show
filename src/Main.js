import React from 'react';
import {ImageBackground, Text, TouchableOpacity, View} from 'react-native';
import styles from './style/MainStyle.js'

const image = {uri:  "https://minimalistphotographyawards.com/wp-content/uploads/2020/07/Hamidreza_Parvizi_Rock-Paper-Scissors01.jpg" }

export default function Main({navigation}) {
    return (
        <ImageBackground source={image} style={styles.image}>
            <View style={styles.container}>
                <View style={styles.butt}>
                    <TouchableOpacity style={styles.playOnline}
                                      onPress={()=> navigation.navigate('Create')}>
                        <Text style={styles.textPlayOnline}>Play Online</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.playComp} title='Comp'
                                      onPress={()=> navigation.navigate('GameComp')}>
                        <Text style={styles.textPlayComp}>Play with Comp</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ImageBackground>
    );
}



/*const token = useContext(ContextGlobal);*/
/* const refreshPage=()=>{
     myToken.reload(false);
 }*/
/*   const refreshPage=()=>{
       myToken().then(json => setToken(json))
       /!*.finally(()=> setLoading(false));*!/

    setTimeout(function () {

   }, 3000);
};*/
