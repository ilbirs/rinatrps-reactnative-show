import React, {useContext} from "react";
import rock from "../assets/img_1.png";
import paper from "../assets/img.png";
import scissors from "../assets/img_2.png";
import {TouchableOpacity, View, Image} from "react-native";
import ContextGlobal from "./context/ContextToken";
import {getMove} from "./api/MyApi";
import {choseFigure} from "./Figure";
import styles from './style/GameStyle.js'

export default function Actions2() {
    const token = useContext(ContextGlobal);

    return (
        <View style={[styles.actions]}>
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => getMove(token,choseFigure("ROCK"))}>
                <Image
                    style={styles.actionButton1}
                    source={rock}/>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => getMove(token,choseFigure("PAPER"))}>
                <Image
                    style={styles.actionButton2}
                    source={paper}/>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => getMove(token,choseFigure("SCISSORS"))}>
                <Image
                    style={styles.actionButton3}
                    source={scissors}
                />
            </TouchableOpacity>

        </View>
    );
}


/*   const Move = (gameMove) => {
     const requestOptions = {
         headers: {
             "token": token
             , "Content-Type": "application/json;"
         },
     };
     fetch(`http://192.168.0.173:8080/games/move?move=${gameMove}`, requestOptions)
         .then((response) => response.json())
         .then((res) => res)
     /!*.catch((error) => console.error(error));*!/
 }*/
