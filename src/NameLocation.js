import React from 'react';
import {useFonts} from "expo-font";
import {Text, View} from "react-native";
import styles from './style/GameStyle.js'

export default function NameLocation({props, props2}) {
    const [loaded] = useFonts({
        Mon: require('../assets/fonts/Monoton-Regular.ttf'),
    });

    if (!loaded) {
        return null;
    }
    return(
        <View style={styles.nameName}>
            <Text style={{fontFamily: 'Mon', fontSize:14}} >{props}</Text>
            <Text style={{fontFamily: 'Mon', fontSize:14}} >{props2}</Text>
        </View>
    )
}

