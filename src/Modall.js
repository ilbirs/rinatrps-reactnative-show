import React, {useContext, useEffect, useState} from 'react';
import ContextGlobal from "./context/ContextToken";
import {deleteByToken, getGameStatus} from "./api/MyApi";
import {Alert, Modal, Pressable, Text, View} from "react-native";
import {Restart} from "fiction-expo-restart";
import styles from './style/GameStyle.js'

export default function Modall() {
    const [gameStatus, setGameStatus] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const token = useContext(ContextGlobal);

    useEffect(() => {
        let mounted = true
        setInterval(() => {
            getGameStatus(token).then((response) => response.json())
                .then(res=> {if(mounted) {setGameStatus(res)}})
        }, 3000);
        return () => {mounted = false}
    }, []);

    if (gameStatus.status === "WIN" || gameStatus.status === "LOSE" || gameStatus.status === "DRAW") {
        setTimeout(function () {
            setModalVisible(true)
        }, 3000);
    }

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);}}>
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <Pressable style={styles.button1}
                        onPress={() => Restart()}>
                        <Text style={styles.textStyle}>Restart</Text>
                    </Pressable>
                    <Pressable style={styles.button2}
                        onPress={() =>{deleteByToken(token); Restart();}}>
                        <Text style={styles.textStyle}>Delete/Restart</Text>
                    </Pressable>
                </View>
            </View>
        </Modal>
    )
}


/*  const delAndRest = () => {
        deleteByToken(token);
        Restart();
    }*/
