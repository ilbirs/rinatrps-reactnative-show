import React from 'react';
import { Text, View} from "react-native";
import styles from './style/NavBarStyle.js'

export const NavBar = (props) =>{
    return(
        <View style={styles.navbar}>
            <Text style={styles.text}>{props.title}</Text>
        </View>
    )
}

