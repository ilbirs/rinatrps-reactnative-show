import React, {useContext, useState} from 'react';
import {ImageBackground, Text, TextInput, TouchableOpacity, View} from "react-native";
import {RectButton} from "react-native-gesture-handler";
import {Feather as Icon} from '@expo/vector-icons';
import ContextGlobal from "./context/ContextToken";
import {createName} from "./api/MyApi";
import styles from './style/CreateStyle.js'

const image = {uri:  "https://previews.123rf.com/images/fergregory/fergregory1209/fergregory120900098/15384897-x-rayed-hands-playing-rock-paper-scissors.jpg" }

export default function Create({navigation,props}) {
    const [nameTillToken, setNameTillToken] = useState("");
    const token = useContext(ContextGlobal)

    const pressToCreateName = () => {
        if(nameTillToken.trim()) {
            navigation.navigate('MotPlayer');
            createName(token, nameTillToken);
            setNameTillToken('')
        }else {
            alert('field can not be blank')
        }
    }

    const pressToPlayAnonymous = () => {
        navigation.navigate('MotPlayer');
        props = {token}
    }

    return(
        <ImageBackground source={image} style={styles.image}>
        <View style={styles.block}>
            <View style={styles.block2}>
                <TextInput style={styles.input}
                     onChangeText={text=> setNameTillToken(text)}
                           value={nameTillToken}
                           placeholder={'Play with Name'}
                           placeholderTextColor='white'
                />
                <RectButton style={styles.button} activeOpacity={0.5} onPress={pressToCreateName} >
                    <Icon name="arrow-right" style={styles.icon}/>
                </RectButton>
            </View>
            <TouchableOpacity style={styles.playOnline}
                              onPress={pressToPlayAnonymous}>
                <Text style={styles.textPlayOnline}>Play Anonymous</Text>
            </TouchableOpacity>
        </View>
        </ImageBackground>
    )
}


/*  const requestOptions = {
      method: "POST",
      header: {
          "token": Cont,
          "Content-Type": "application/json;"
      },
      body: JSON.stringify({'name': 'diako'}),

  };
  fetch(`http://localhost:8080/user/name`, requestOptions)
      .then(response => response.json())
      .then(res => setT(res))
      .catch((error) => console.error(error))*/

/* const addTodo = (title)=>{
     setTodos(prev => [...prev,{
         id:dataa,
         title
     }])
 }*/
/* const addTodo = ()=>{
     setTodos(prev => [...prev,{
         id:Cont,
     }])
 }*/
/* const addTodo2 = ()=>{
     setT(prev => [...prev,{
         name:t
     }])
 }*/
/*console.log('ee', todos)*/
/* const itemInputHandler = (text)=>{
      setOutputText(text);
  }*/

/*  const pressHandler =()=>{
       navigation.navigate('GamePage');
      re(outputText);
      setOutputText('')
      console.log("ffffffffff", outputText)
  }*/
{/*<PlayerName todo={submitText} onClickk={()=> navigation.navigate('GamePage')} />*/}


{/* <NameInput  onClickk={()=> navigation.navigate('GamePage')} />*/}


{/*    <FlatList
                keyExtractor={item => item.id}
                data={todos}
                renderItem={({item}) => <PlayerName todo={item}
                                             />
                }
            />*/}
{/*  <Text style={{color:'black'}}>{Cont}</Text>*/}

{/*<Button title='fff' onPress={pressHandler}/>*/}
/* const dd =()=> {
    return  createName.then((res) => res)
 }*/
/*  const pressHandler = () => {
      const requestOptions = {
          method: "POST",
          body: nameTillToken,
          headers: {
              "token": token
              , "Content-Type": "application/json;"
          },
      };
      fetch(`http://192.168.0.173:8080/user/name`, requestOptions)
          .then((response) => response.json())
          .then((res) => setSubmitText(res))
          .catch((error) => console.error(error))

  }*/
/*   console.log('info',submitText)*/
