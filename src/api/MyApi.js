import React from 'react';

export const myToken = () =>
     fetch('http://192.168.0.101:8080/auth/token')
        .then((response) => response.text())
         .then((json) => {
             console.log("min token är: ",json)
             return json
         })
        .catch((error) => console.error(error))


export const validateToken = (token) =>
     fetch(`http://192.168.0.101:8080/auth/validate/${token}`)
        .then((response) => response.text())
        .catch((error) => console.error(error))


export const createName = (token, nameTillToken) => {
    const requestOptions = {
        method: "POST",
        body: nameTillToken,
        headers: {
            "token": token
            , "Content-Type": "application/json;"
        },
    };
    fetch(`http://192.168.0.101:8080/user/name`, requestOptions)
        .then((response) => response.json())
        .then((res) => {
            console.log("name :",res)
            return res
        })
        .catch((error) => console.error(error))
}

export const startGame = (token) => {
    const requestOptions = {
        headers: {
            "token": token
            , "Content-Type": "application/json;"
        },
    };
    fetch(`http://192.168.0.101:8080/games/start`,requestOptions)
        .then((response) => response.json())
        .then((res) =>{
            console.log("game Id :",res)
            return res
        })
        .catch((error) => console.error(error))
}

export const getGameStatus = (token) =>  fetch('http://192.168.0.101:8080/games/status',
    {
        method: "GET",
        headers: {
            "token": token
            , "Content-Type": "application/json;"
        },
})

export const getGameList = (token) => fetch('http://192.168.0.101:8080/games', {
    method: "GET",
    headers: {
        "token": token
        , "Content-Type": "application/json;"
    },
})

export const joinGame = (token,gameId) => {
    const requestOptions = {
        headers: {
            "token": token
            , "Content-Type": "application/json;"
        },
    };
    fetch(`http://192.168.0.101:8080/games/join/${gameId}`, requestOptions)
        .then((response) => response.json())
        .then((res) => res);
}

export const getMove = (token, gameMove) => {
    const requestOptions = {
        headers: {
            "token": token
            , "Content-Type": "application/json;"
        },
    };
    fetch(`http://192.168.0.101:8080/games/move?move=${gameMove}`, requestOptions)
        .then((response) => response.json())
        .then((res) => res)
}


export const deleteByToken = (token) => {
    const requestOptions = {
        method: "DELETE",
        headers: {
            "token": token
            , "Content-Type": "application/json;"
        },
    };
    fetch(`http://192.168.0.101:8080/user/delete/${token}`, requestOptions)
        .then((response) => response.json())
        .then((res) => res)
        /*.catch((error) => console.error(error))*/
}



/*export const getGameStatus = (token) => {
    const requestOptions = {
        method: "GET",expo start

        headers: {
            "token": token
            , "Content-Type": "application/json;"
        },
    };
    fetch('http://192.168.0.173:8080/games/status', requestOptions)
        .then((response) => response.json())
        /!*.then((res) => res)*!/
        .catch((error) => console.error(error))
}*/
/*.catch((error) => console.error(error));*/

/*   .then((json) => {
             console.log("min token är: ",json)
             return json
         })*/
