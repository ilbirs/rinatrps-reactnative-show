import React from 'react';
import question from "../assets/img_5.png";
import rock from "../assets/img_1.png";
import paper from "../assets/img.png";
import scissors from "../assets/img_2.png";

const image = {uri:  "https://media.tenor.com/images/fa2c41d7c0baa3ff0863ee52b8fab514/tenor.gif"}

export const choseFigure = (type) => {
    switch (type) {
        case "ROCK":
            return "ROCK";
        case "PAPER":
            return "PAPER";
        case "SCISSORS":
            return "SCISSORS";
        default:
            return question;
    }
};

export const choseFigure2 = (type) => {
    switch (type) {
        case "ROCK":
            return rock;
        case "PAPER":
            return paper;
        case "SCISSORS":
            return scissors;
        default:
            return image;
    }
};
