import React, {useContext, useEffect, useState} from 'react';
import { SafeAreaView, Text, View} from 'react-native';
import DisplayResult2 from "./DisplayResult2";
import Action2 from "./Action2";
import ContextGlobal from "./context/ContextToken";
import { getGameStatus} from "./api/MyApi";
import { useFonts } from 'expo-font';
import Modall from "./Modall";
import Result from "./Result";
import NameLocation from "./NameLocation";
import {choseFigure2} from "./Figure";
import styles from './style/GameStyle.js'

const image = {uri:  "https://media.tenor.com/images/fa2c41d7c0baa3ff0863ee52b8fab514/tenor.gif"}

export default function Game() {
    const [gameStatus, setGameStatus] = useState([]);
    const token = useContext(ContextGlobal);
    const [loaded2] = useFonts({
        Oi: require('../assets/fonts/CinzelDecorative-Regular.ttf'),
    });

   useEffect(() => {
       let mounted = true
       setInterval(() => {
      getGameStatus(token).then((response) => response.json())
          .then(res=> {
              if(mounted) {
              setGameStatus(res)}})
       }, 3000);
      return () => {
           mounted = false
       }
    }, []);
    console.log('gameStatus', gameStatus)

    if (!loaded2) {
        return null;
    }
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.content}>
                    <Result props={gameStatus.status}/>
                    <NameLocation props={gameStatus.player ? gameStatus.player : 'anonymous'}
                                  props2={gameStatus.opponent === null ? 'anonymous' : gameStatus.opponent}/>
                    <Modall/>
                    <View style={styles.screen}>
                        {gameStatus.opeponentMove === null ?
                            <DisplayResult2 userChoice={image}/> :
                            <DisplayResult2 userChoice={choseFigure2(gameStatus.playerMove)}/>
                        }

                        <View style={styles.designYou}>
                        {gameStatus.playerMove ?
                                <Text style={styles.playerImpl}>implemented</Text> :
                                <Text style={styles.playerYou}>You</Text>}
                            </View>

                        {gameStatus.playerMove === null ?
                            <DisplayResult2 userChoice={image}/> :
                            <DisplayResult2 userChoice={choseFigure2(gameStatus.opponentMove)}/>
                        }

                        <View style={styles.designOpponent}>
                        {gameStatus.opponentMove ?
                                <Text style={styles.playerImpl}>implemented</Text> :
                                <Text style={styles.playerOpponent}>Opponent</Text>}
                            </View>
                    </View>
                    <Action2/>

                </View>
            </SafeAreaView>
        );
}


/*const [todos, setTodos] = useState([]);
const Cont = useContext(ContextGlobal)*/
/* const [names, setNames] = useState('abba');
  const addName =(name) =>{
      setNames(name)
  }*/

/*
const addTodo = (name)=>{
    setTodos(prev => [...prev,{
        id:Cont,
        name
    }])
}
console.log('ee', todos)
   onChangeText={text=> setNameTillToken(text)}
*/

{/*<View style={[styles.actions]}>
                    <TouchableOpacity
                        activeOpacity={0.5}

                        onPress={() => Move(choseFigure("ROCK"))}
                    >
                        <Image
                            style={styles.actionButton1}
                            source={rock}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity

                        onPress={() => Move(choseFigure("PAPER"))}
                    >
                        <Image
                            style={styles.actionButton2}
                            source={paper}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => Move(choseFigure("SCISSORS"))}
                    >
                        <Image
                            style={styles.actionButton3}
                            source={scissors}
                        />
                    </TouchableOpacity>

                </View>*/}

{/*   <View style={styles.column}>
                        <Image
                            style={styles.actionButton}
                            source={choseFigure2(gameStatus.playerMove)}
                        />
                        <Text style={styles.playerName}>You</Text>
                    </View>
                    <View style={styles.column}>
                        <Image
                            style={styles.actionButton}
                            source={choseFigure2(gameStatus.opponentMove)}
                        />
                        <Text style={styles.playerName}>Computer</Text>
                    </View>*/}

/*  const pressHandler4 =() => {
      if (!game ) {
          pressHandler3()
      }
      else if (game) {
          alert('Game already created')
      }*/
/* else {
     //ios  Alert.alert('field can not be blank')
     alert('field can not be blank')
 }
}*/

/*  const Move = (gameMove) => {
       const requestOptions = {
           headers: {
               "token": token
               , "Content-Type": "application/json;"
           },
       };
       fetch(`http://192.168.0.173:8080/games/move?move=${gameMove}`, requestOptions)
           .then((response) => response.json())
           .then((res) => res)
      /!* .catch((error) => console.error(error));*!/



<Text>{gameStatus.player ? gameStatus.player : 'anonymous'}</Text>:

      <ActivityIndicator style={styles.column} size="large" color="#00ff00" />

   }*/
/*<Button
                    title="Start Game"
                onPress = { () => pressHandler4() }
                />*/

/* {gameStatus.status === "WIN" || gameStatus.status === "LOSE" || gameStatus.status === "DRAW" ?
                        <Text style={styles.resultText}>{interv()}</Text> :*/

/*    const fetchFont =async () => {
        await Font.loadAsync({
            DancingScrip: require('../assets/fonts/DancingScript-VariableFont_wght.ttf')
        });
    }*/
/*   if(!fontLoaded) {
       return (
       <AppLoading
           startAsync={fetchFont}
           onError = {()=> console.log('ffffff')}
           onFinish ={()=> {
               setFontLoaded(true);
           }}
           />
       );
   }
*/
/*  let [fontsLoaded] = useFonts({
      'Inter-Black': require('../assets/fonts/DancingScript-VariableFont_wght.ttf'),
  });*/
/*import {
    useFonts,
    Akronim_400Regular,
} from '@expo-google-fonts/akronim'*/

/*const fetchFont = () => {
     Font.loadAsync({
        DancingScrip: require('../assets/fonts/DancingScript-VariableFont_wght.ttf')
    });
}*/
/*  let [fontsLoaded] = useFonts({
      Akronim_400Regular
  });
  if (!fontsLoaded) {
      return <AppLoading />;
  } else {*/

/* if (!loaded2) {
        return null;
    }*/
/* <View style={styles.result}>
                        <Text style={styles.resultText}>{gameStatus.status}</Text>
                    </View>*/
/*  <View style={styles.nameName}>
                        <Text
                            style={{fontFamily: 'Mon', fontSize:14}} >{gameStatus.player ? gameStatus.player : 'anonymous'}</Text>
                        <Text
                            style={{fontFamily: 'Mon',fontSize:14}}>{gameStatus.opponent === null ? 'anonymous' : gameStatus.opponent}</Text>
                    </View>*/
