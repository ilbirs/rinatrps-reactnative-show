import React, { useState} from 'react';
import { SafeAreaView, StyleSheet, Text, View} from 'react-native';
import DisplayResult from "./DisplayResult";
import Actions from "./Actions";

export default function GameComp() {
    const [userChoice, setUserChoice] = useState(0);
    const [computerChoice, setComputerChoice] = useState(0);
    const [result, setResult] = useState("");

    function play(choice) {
        const randomComputerChoice = Math.floor(Math.random() * 3) + 1;
        let resultString = "";
        if (choice === 1) {
            resultString = randomComputerChoice === 3 ? "WIN" : "LOSE";
        } else if (choice === 2) {
            resultString = randomComputerChoice === 1 ? "WIN" : "LOSE";
        } else {
            resultString = randomComputerChoice === 2 ? "WIN" : "LOSE";
        }
        if (choice === randomComputerChoice) {
            resultString = "It is DRAW";
        }
        setUserChoice(choice);
        setComputerChoice(randomComputerChoice);
        setTimeout(() => {
            setResult(resultString);
        }, 1000);
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.content}>
                <View style={styles.result}>
                    <Text style={styles.resultText}>{result}</Text>
                </View>
                <View style={styles.screen}>
                    {!result ? (
                        <Text style={styles.readyText}>Vi Kör!!</Text>
                    ) : (
                       <DisplayResult
                            userChoice={userChoice}
                            computerChoice={computerChoice}
                        />
                    )}
                </View>
                <Actions play={play} />
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        padding: 15,
    },
    result: {
        height: 70,
        justifyContent: "flex-end",
        alignItems: "center",
        borderWidth:1
    },
    resultText: {
        fontSize: 30,
        color:'black',
        top:-15
    },
    screen: {
        flex: 1,
        flexDirection: "row",
    },
    readyText: {
        marginTop: -48,
        alignSelf: "center",
        textAlign: "center",
        width: "100%",
        fontSize: 48,
        fontWeight: "bold",
    },
});
