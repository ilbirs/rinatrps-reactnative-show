import React from "react";
import rock from "../../assets/img_1.png";
import paper from "../../assets/img.png";
import scissors from "../../assets/img_2.png";
import { TouchableOpacity, StyleSheet, View, Image} from "react-native";

export default function Actions({ play }) {
    return (
        <View style={[styles.actions]}>
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => play(1)}>
                <Image
                    style={styles.actionButton1}
                    source={rock}/>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => play(2)}>
                <Image
                    style={styles.actionButton2}
                    source={paper}/>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => play(3)}>
                <Image
                    style={styles.actionButton3}
                    source={scissors}/>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    actions: {
        top:-40,
        height: 100,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        borderBottomWidth:2
    },
    actionButton1: {
        width: 64,
        height: 64,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 32,
        borderColor:'blue',
        borderWidth:7,
    },
    actionButton2: {
        width: 64,
        height: 64,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 32,
        borderColor:'green',
        borderWidth:7
    },
    actionButton3: {
        width: 64,
        height: 64,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 32,
        borderColor:'red',
        borderWidth:7
    },
});
