import React from "react";
import {Image, StyleSheet, Text, View} from "react-native";

import rock from "../../assets/img_1.png";
import paper from "../../assets/img.png";
import scissors from "../../assets/img_2.png";
import question from "../../assets/img_5.png";

export default function DisplayResult({userChoice, computerChoice }) {
    const choseFigure = (type) => {
        switch (type) {
            case 1:
                return rock;
            case 2:
                return paper;
            case 3:
                return scissors;
            default:
                return question;
        }
    };

    return (
        <>
            <View style={styles.column}>
                <Image
                    style={styles.actionButton}
                    source={choseFigure(userChoice)}
                />
                <Text style={styles.playerName}>You</Text>
            </View>
            <View style={styles.column}>
                <Image
                    style={styles.actionButton}
                    source={choseFigure(computerChoice)}
                />
                <Text style={styles.playerName}>Computer</Text>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    column: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    playerName: {
        color: "black",
        fontSize: 16,
        marginTop: 16,
    },
    actionButton: {
        width: 100,
        height: 100,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#f9d835",
       borderRadius: 52,
    },
});
