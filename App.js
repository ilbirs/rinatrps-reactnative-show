
import React, {useEffect, useState} from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from "@react-navigation/stack";
import GameMotPlayer from "./src/GameMotPlayer";
import {NavBar} from "./src/NavBar";
import Create from "./src/Create";
import Join from "./src/Join";
import Game from "./src/Game";
import Main from "./src/Main";
import GameComp from "./src/comp/GameComp";
import ContextGlobal from "./src/context/ContextToken";
import {myToken, validateToken} from "./src/api/MyApi";
import * as SecureStore from "expo-secure-store";

const Stack = createStackNavigator()

async function saveTokenToSecureStore(token) {
    await SecureStore.setItemAsync('token', token);
}

async function getTokenFromSecureStore() {
    return SecureStore.getItemAsync('token');

}

export default function App() {
    const [token, setToken] = useState('');

    function fetchNewToken() {
        myToken().then(newToken => {
            setToken(newToken)
            saveTokenToSecureStore(newToken)
        })
    }

    useEffect(() => {
         getTokenFromSecureStore()
            .then(secureToken => {
                if(!!secureToken){
                    validateToken(secureToken)
                        .then(isValid =>{
                            if(isValid)
                                setToken(secureToken)
                            else
                                alert(" token not valid, request a new token ");
                                fetchNewToken();
                        })
                    setToken(secureToken)
                } else {
                    fetchNewToken();
                }
            })
            .catch(()=>{
                fetchNewToken();
            })
    }, []);

  return (
      <ContextGlobal.Provider value={token}>
      <NavigationContainer>
          <NavBar title='Rock-Paper-Scissors'/>
          <Stack.Navigator>
              <Stack.Screen name='Main' component={Main}/>
              <Stack.Screen name='GameComp' component={GameComp}/>
              <Stack.Screen name='MotPlayer' component={GameMotPlayer}/>
              <Stack.Screen name='Create' component={Create}/>
              <Stack.Screen name='Join' component={Join}/>
              <Stack.Screen name='Game' component={Game}/>
          </Stack.Navigator>
      </NavigationContainer>
      </ContextGlobal.Provider>
  );
}

/*
    const requestOptions = {
        method: "POST",
        header: {
            "token": this.token,
            "Content-Type": "application/json;"
        },
        body: JSON.stringify({'name': 'diako'}),

    };
    fetch(`http://localhost:8080/user/name`, requestOptions)
        .then(response => response.json())
        .then(res => setDataa(res))
        .catch((error) => console.error(error))*/


/*  useEffect(() => {
       fetch('http://192.168.0.173:8080/auth/token')
           .then((response) => response.text())
           .then((json) => setDataa(json))
           .catch((error) => console.error(error))
   }, []);*/
